#!/usr/bin/env bash

php bin/console doctrine:query:sql "SET foreign_key_checks = 0"
php bin/console doctrine:fixtures:load --purge-with-truncate -n
php bin/console doctrine:query:sql "SET foreign_key_checks = 1"

