<?php

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{
    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('homepage'));
    }


    /**
     * @When /^я нажимаю на кнопку "Выход"$/
     */
    public function яВижуКнопкуИНажимаю() {
        $this->clickLink('Выход');
    }


    /**
     * @When /^я вижу ссылку "Авторизация" и нажимаю на нее$/
     */
    public function яВижуСсылкуАвторизацияИНажимаюНаНее() {
        $this->clickLink('Авторизация');
    }

    /**
     * @When /^я вижу форму авторизации и ввожу данные в поля "([^"]*)" и "([^"]*)",потом кликаю на кнопку "([^"]*)"$/
     */
    public function яВижуФормуАвторизацииИАвторизуюсь($field1,$field2,$button) {
        $this->fillField($field1, "qwe@qwe.qwe");
        $this->fillField($field2, "1123581321");
        $this->pressButton($button);
    }

    /**
     * @When /^я вижу форму регистрации и ввожу данные в поля ,потом кликаю на кнопку "([^"]*)"$/
     */
    public function яВижуФормуРегистрацииИРегитсрируюсь($button) {
        $this->fillField('Ваш емайл:', "akbarov@ak.ru");
        $this->fillField('Ваши пасспортные данные:', "AN567");
        $this->fillField('Ваш пароль:', "112");
        $field = $this->getSession()->getPage()->findField('Выберите кто вы');
        $field->selectOption('Арендатор');
        $this->pressButton($button);
    }

    /**
     * @When /^я вижу ссылку "Регистриция" и нажимаю на нее$/
     */
    public function яВижуСсылкуРегистрацияИНажимаюНаНее() {
        $this->clickLink('Регистрация');
    }

    /**
     * @When /^я заполняю поля формы поиска и жму поиск$/
     */
    public function яВижуФормуПоискаИИщуПансионат() {
        $this->fillField('Введите слово', "Иссык Куль");
        $this->fillField('Введите минимальную цену', "300");
        $this->pressButton('Поиск');
    }


}
