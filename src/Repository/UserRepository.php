<?php

namespace App\Repository;

use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    private $userHandler;

    public function __construct(RegistryInterface $registry, UserHandler $userHandler)
    {
        parent::__construct($registry, User::class);
        $this->userHandler = $userHandler;
    }

    public function findByPasswordAndEmail(string $email, string $password)
    {
        $passwordHash = $this->userHandler->encodePassword($password);
        try {
            return $this->createQueryBuilder('u')
                ->select('u')
                ->where('u.password = :password')
                ->andWhere('u.email = :email')
                ->setParameter('password', $passwordHash)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }

    /**
     * @param string $passport
     * @param string $email
     * @return User|null
     */
    public function findOneByPassportAndEmail(string $passport, string $email) : ?User
    {
        try {
            return $this->createQueryBuilder('u')
                ->select('u')
                ->where('u.passport = :passport')
                ->andWhere('u.email = :email')
                ->setParameter('passport', $passport)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByUid(string $network, string $uid)
    {
        try {
            $qb = $this->createQueryBuilder('u')
                ->select('u');
            switch ($network) {
                case UserHandler::SOC_NETWORK_VKONTAKTE:
                    $qb->where("u.vkId = :uid");
                    break;
                case UserHandler::SOC_NETWORK_FACEBOOK:
                    $qb->where("u.faceBookId = :uid");
                    break;
                case UserHandler::SOC_NETWORK_GOOGLE:
                    $qb->where("u.googleId = :uid");
                    break;
            }
            return $qb->setParameter('uid', $uid)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
