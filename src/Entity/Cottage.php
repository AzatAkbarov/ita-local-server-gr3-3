<?php


namespace App\Entity;

class Cottage extends BookingObject
{
    /**
     * @var boolean
     */
    private $cook;

    /**
     * @var boolean
     */
    private $sauna;

    public function setCook($cook) {

        $this->cook = $cook;
        return $this;
    }

    public function getCook() {

        return $this->cook;
    }

    public function setSauna($sauna) {

        $this->sauna = $sauna;
        return $this;
    }

    public function getSauna() {

        return $this->sauna;
    }

    public function __toArray() {

        return [
            'type' => $this->getType(),
            'owner' => $this->getOwner(),
            'name' => $this->getName(),
            'quantity' => $this->getQuantity(),
            'contact_person' => $this->getContactPerson(),
            'phone_number' => $this->getPhoneNumber(),
            'coordinates' => $this->getCoordinates(),
            'price' => $this->getPrice(),
            'cook' => $this->cook,
            'sauna' => $this->sauna
        ];
    }

}