<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/2/18
 * Time: 2:31 PM
 */

namespace App\Entity;


class Pension extends BookingObject
{
    /**
     * @var boolean
     */
    private $three_meals_a_day;

    /**
     * @var boolean
     */
    private $mud_bath;

    public function setThreeMealsADay($three_meals_a_day) {

        $this->three_meals_a_day = $three_meals_a_day;
        return $this;
    }

    public function getThreeMealsADay() {

        return $this->three_meals_a_day;
    }

    public function setMudBath($mud_bath) {

        $this->mud_bath = $mud_bath;
        return $this;
    }

    public function getMudBath() {

        return $this->mud_bath;
    }

    public function __toArray() {

        return [
            'type' => $this->getType(),
            'owner' => $this->getOwner(),
            'name' => $this->getName(),
            'quantity' => $this->getQuantity(),
            'contact_person' => $this->getContactPerson(),
            'phone_number' => $this->getPhoneNumber(),
            'coordinates' => $this->getCoordinates(),
            'price' => $this->getPrice(),
            'three_meals_a_day' => $this->three_meals_a_day,
            'mud_bath' => $this->mud_bath
        ];
    }
}