<?php


namespace App\Entity;


/**
 * This is not entity, just a class
 */
class BookingObject
{
    protected $id;

    protected $type;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * @var string
     */
    protected $contact_person;

    /**
     * @var string
     */
    protected $phone_number;

    /**
     * @var string
     */
    protected $owner;

    /**
     * @var string
     */
    protected $coordinates;

    /**
     * @var float
     */
    protected $price;

    public function setName($name) {

        $this->name = $name;
        return $this;
    }

    public function getName() {

        return $this->name;
    }

    public function setQuantity($quantity) {

        $this->quantity = $quantity;
        return $this;
    }

    public function getQuantity() {

        return $this->quantity;
    }

    public function setContactPerson($contact_person) {

        $this->contact_person = $contact_person;
        return $this;
    }

    public function getContactPerson() {

        return $this->contact_person;
    }

    public function setPhoneNumber($phone_number) {

        $this->phone_number = $phone_number;
        return $this;
    }

    public function getPhoneNumber() {

        return $this->phone_number;
    }

    public function setCoordinates($coordinates) {

        $this->coordinates = $coordinates;
        return $this;
    }

    public function getCoordinates() {

        return $this->coordinates;
    }

    public function setPrice($price) {

        $this->price = $price;
        return $this;
    }

    public function getPrice() {

        return $this->price;
    }

    public function setType($type) {

        $this->type = $type;
        return $this;
    }

    public function getType() {

        return $this->type;
    }

    public function setId($id) {

        $this->id = $id;
        return $this;
    }

    public function getId() {

        return $this->id;
    }

    /**
     * @param $owner
     * @return $this
     */
    public function setOwner($owner) {

        $this->owner = $owner;
        return $this;
    }

    /**
     * @return string
     */
    public function getOwner() {

        return $this->owner;
    }

}