<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 6/19/18
 * Time: 12:42 PM
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('word',TextType::class, [
            'label' => 'Поиск по слову',
            'required' => false,
            'attr' => [
                'placeholder' => 'Введите слово'
            ]
        ])
            ->add('from', IntegerType::class, [
                'label' => 'Цена от',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Введите минимальную цену'
                ]
            ])
            ->add('before', IntegerType::class, [
                'label' => 'Цена до',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Введите максимальную цену'
                ]
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Выберите тип объекта',
                'required' => false,
                'placeholder' => 'Выберите один из вариантов',
                'choices' => [
                    'Пансионат' => 'Pension',
                    'Коттедж' => 'Cottage'
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Поиск'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_search_type';
    }

}