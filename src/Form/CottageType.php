<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CottageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cook', ChoiceType::class, [
            'label' => 'Наличие повара',
            'placeholder' => 'Выберите один из 2 вариантов',
            'choices' => [
                'Да' => true,
                'Нет' => false
            ]
        ])
            ->add('sauna', ChoiceType::class, [
                'label' => 'Наличие сауны',
                'placeholder' => 'Выберите один из 2 вариантов',
                'choices' => [
                    'Да' => true,
                    'Нет' => false
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Зарегистрировать коттедж'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_cottage_type';
    }

}