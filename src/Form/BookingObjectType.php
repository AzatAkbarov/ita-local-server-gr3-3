<?php


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BookingObjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', ChoiceType::class, [
            'label' => 'Выберите тип объекта',
            'placeholder' => 'Выберите один из 2 вариантов',
            'choices' => [
                'Пансионат' => 'pension',
                'Коттедж' => 'cottage'
            ]
        ])
            ->add('name', TextType::class,[
                'label' => "Название объекта:",
                'attr' => [
                    'placeholder' => 'Введите название'
                ]
            ])
            ->add('quantity', NumberType::class,[
                'label' => "Количество номеров:",
                'attr' => [
                    'placeholder' => 'Введите количество номеров'
                ]
            ])
            ->add('contact_person', TextType::class,[
                'label' => "Контактное лицо:",
                'attr' => [
                    'placeholder' => 'Введите ФИО контактного лица'
                ]
            ])
            ->add('phone_number', TelType::class,[
                'label' => "Номер телефона в формате 0xxx-xx-xx-xx",
                'attr' => [
                    'pattern' => "0[0-9]{3}-[0-9]{2}-[0-9]{2}-[0-9]{2}"
                ]
            ])
            ->add('coordinates', TextType::class,[
                'label' => 'Координаты (Поставьте метку на карте)',
                'attr' => [
                    'placeholder' => 'Поставьте метку на карте!',
                    'readonly' => true
                ]
            ])
            ->add('price', NumberType::class,[
                'label' => "Цена одного номера за сутки:",
                'attr' => [
                    'placeholder' => 'Введите цену'
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Далее'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_object_type';
    }

}