<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/2/18
 * Time: 4:05 PM
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PensionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('three_meals_a_day', ChoiceType::class, [
            'label' => 'Наличие 3-х разового питания',
            'placeholder' => 'Выберите один из 2 вариантов',
            'choices' => [
                'Да' => true,
                'Нет' => false
            ]
        ])
            ->add('mud_bath', ChoiceType::class, [
                'label' => 'Наличие грязевых ванн',
                'placeholder' => 'Выберите один из 2 вариантов',
                'choices' => [
                    'Да' => true,
                    'Нет' => false
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Зарегистрировать пансионат'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_pension_type';
    }

}