<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username',EmailType::class, [
            'label' => 'Ваш емайл:',
            'attr' => [
                'placeholder' => 'Введите адрес Вашей электронной почты'
            ]
        ])
            ->add('passport', TextType::class,[
                'label' => "Ваши пасспортные данные:",
                'attr' => [
                    'placeholder' => 'Введите Ваши пасспортные данные'
                ]
            ])
            ->add('password', PasswordType::class,[
                'label' => "Ваш пароль:",
                'attr' => [
                    'placeholder' => 'Введите пароль'
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'Выберите кто вы',
                'placeholder' => 'Выберите один из 2 вариантов',
                'choices' => [
                    'Арендатор' => 'tenant',
                    'Арендодатель' => 'landlord'
                    ]
                  ])
            ->add('save', SubmitType::class, [
                'label' => 'Зарегистрироваться'
            ]);

        /** @var User $user */
        $user = $builder->getData();

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($storedRoles) use (&$user) {
                    $currentRoles = $user->getRoles();
                    if (in_array('ROLE_TENANT', $currentRoles)) {
                        return 'tenant';
                    } elseif (in_array('ROLE_LANDLORD', $currentRoles)) {
                        return 'landlord';
                    } else {
                        return null;
                    }
                },
                function ($tagsAsString) use (&$user) {
                    $currentRoles = array_diff($user->getRoles(), [
                        'ROLE_TENANT',
                        'ROLE_LANDLORD',
                    ]);

                    switch ($tagsAsString) {
                        case 'tenant':
                            $currentRoles[] = 'ROLE_TENANT';
                            break;
                        case 'landlord':
                            $currentRoles[] = 'ROLE_LANDLORD';
                            break;
                        default:
                            $currentRoles = $user->getRoles();
                            break;
                    }

                    return $currentRoles;
                }
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_register_type';
    }
}
