<?php

namespace App\Controller;

use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\BookingObject\BookingObjectHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/booking")
 */
class BookingController extends Controller
{
    const BOOKING_OBJECT = 'booking_object';

    /**
     * @Route("/registration_new_object", name="registration_new_object")
     * @param Session $session
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registrationNewObject(Session $session, Request $request)
    {
        if (!$this->getUser()) {
            $session->getFlashBag()->set('error', 'Зарегистрируйтесь как арендодатель и Вы сможете регистрировать объекты бронирования');
            return $this->redirectToRoute('homepage');
        } else {
            if ($this->isGranted('ROLE_TENANT', $this->getUser())) {
                $session->getFlashBag()->set('error', 'Вы арендатор!Вы не можете регистрировать объекты!');
                return $this->redirectToRoute('homepage');
            }
        }
        $object = new BookingObject();
        $form = $this->createForm("App\Form\BookingObjectType", $object);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $object = $form->getData();
            if (empty($object->getCoordinates())) {
                $session->getFlashBag()->set('error', 'Вы не указали на карте метку(местоположение объекта на карте)');
                return $this->redirectToRoute('registration_new_object');
            }
            $this->get('session')->set(self::BOOKING_OBJECT, $object);

            return $this->redirectToRoute('registration_new_type_object');
        }

        return $this->render('registration_new_object.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/booking_object_details", name="booking_object_details")
     * @param Session $session
     * @param $id
     * @param ApiContext $apiContext
     * @param BookingObjectHandler $bookingObjectHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function bookingObjectDetailsAction(Session $session, $id, ApiContext $apiContext, BookingObjectHandler $bookingObjectHandler)
    {
        try {
            $objectData = $apiContext->getObjectById($id);
            $bookings = $apiContext->getBookingsByObjectId($id);
            $object = $bookingObjectHandler->createNewBookingObject($objectData);


            return $this->render('show_object_details.html.twig', [
                'object' => $object,
                'bookings' => $bookings
            ]);
        } catch (ApiException $e) {
            $session->getFlashBag()->set('error', 'Что то пошло не так!');
            return $this->redirectToRoute('homepage');
        }
    }

    /**
     * @Route("/registration_new_type_object", name="registration_new_type_object")
     * @param Session $session
     * @param Request $request
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registrationNewTypeObject(Session $session, Request $request, ApiContext $apiContext)
    {
        if (!$this->getUser()) {
            $session->getFlashBag()->set('error', 'Зарегистрируйтесь как арендодатель и Вы сможете регистрировать объекты бронирования');
            return $this->redirectToRoute('homepage');
        } else {
            if ($this->isGranted('ROLE_TENANT', $this->getUser())) {
                $session->getFlashBag()->set('error', 'Вы арендатор!Вы не можете регистрировать объекты!');
                return $this->redirectToRoute('homepage');
            }
        }
        $error = null;

        /**
         * @var BookingObject $object
         */
        $object = $this
            ->get('session')
            ->get(self::BOOKING_OBJECT);

        $typeOfObject = $object->getType();
        $form = null;

        switch ($typeOfObject) {
            case 'pension':
                $pension = new Pension();
                $form = $form = $this->createForm("App\Form\PensionType", $pension);
                break;
            case 'cottage':
                $cottage = new Cottage();
                $form = $form = $this->createForm("App\Form\CottageType", $cottage);
                break;
            default:
                $error = 'Что то явно пошло не так!';
                break;
        }

        if (!empty($error)) {
            $session->getFlashBag()->set('error', $error);
            return $this->redirectToRoute('homepage');
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var User $currentUser
             */
            $currentUser = $this->getUser();
            $objectToSave = $form->getData();
            $objectToSave
                ->setType($object->getType())
                ->setOwner($currentUser->getPassport())
                ->setName($object->getName())
                ->setQuantity($object->getQuantity())
                ->setContactPerson($object->getContactPerson())
                ->setPhoneNumber($object->getPhoneNumber())
                ->setCoordinates($object->getCoordinates())
                ->setPrice($object->getPrice());

            $dataToSaveOnCentral = $objectToSave->__toArray();

            try {
                $apiContext->createNewObject($dataToSaveOnCentral);
                $session->getFlashBag()->set('message', 'Вы успешно создали объект');
            } catch (ApiException $e) {
                $error = $e->getResponse()['error'];
                $session->getFlashBag()->set('error', $error);
            }
            return $this->redirectToRoute('homepage');
        }
        return $this->render('registration_new_type_object.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/book_a_room/{objectId}/{roomNumber}", name="book_a_room")
     * @param Session $session
     * @param Request $request
     * @param $objectId
     * @param $roomNumber
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws ApiException
     */
    public function bookARoomAction(Session $session, Request $request, $objectId, $roomNumber, ApiContext $apiContext)
    {
        if (!$this->getUser()) {
            $session->getFlashBag()->set('error', 'Зарегистрируйтесть как арендатор и Вы сможете бронировать номера в объектах');
        } else {
            if ($this->isGranted('ROLE_LANDLORD', $this->getUser())) {
                $session->getFlashBag()->set('error', 'Вы арендодатель и Вы не можете бронировать номера в объектах');
            } else {
                /** @var User $currentUser */
                $currentUser = $this->getUser();
                if ($apiContext->checkBookingByParam($currentUser->getPassport())) {
                    $session->getFlashBag()->set('error', 'Вы уже бронировали комнату');
                } else {
                    try {
                        $data = [
                            'objectId' => $objectId,
                            'roomNumber' => $roomNumber,
                            'passport' => $currentUser->getPassport()
                        ];
                        $apiContext->createBookingByParams($data);
                        $session->getFlashBag()->set('message', 'Вы успешно забронировали номер');
                    } catch (ApiException $e) {
                        $error = $e->getResponse()['error'];
                        $session->getFlashBag()->set('error', $error);
                    }
                }
            }
        }
        return $this->redirect(
            $request
                ->headers
                ->get('referer')
        );
    }

}