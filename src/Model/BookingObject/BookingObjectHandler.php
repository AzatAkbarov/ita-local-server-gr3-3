<?php

namespace App\Model\BookingObject;


use App\Entity\Cottage;
use App\Entity\Pension;

class BookingObjectHandler
{
    const TYPE_COTTAGE = "cottage";
    const TYPE_PENSION = "pension";

    const NAME_COTTAGE = "Коттедж";
    const NAME_PENSION = "Пансионат";

    const COTTAGE_FULL_NAME = "App\Entity\Cottage";
    const PENSION_FULL_NAME = "App\Entity\Pension";


    /**
     * @param array $data
     * @return mixed
     */
    public function createNewBookingObject(array $data) {
        $type = $data['type'];
        $object = null;
        switch ($type) {
            case self::COTTAGE_FULL_NAME:
                $object = $this->createNewCottage($data);
                break;
            case self::PENSION_FULL_NAME:
                $object = $this->createNewPension($data);
                break;
            default:
                $object = null;
        }
        $object
            ->setId($data['id'])
            ->setName($data['name'])
            ->setQuantity($data['quantity'])
            ->setContactPerson($data['contact_person'])
            ->setOwner($data['owner'])
            ->setPhoneNumber($data['phone_number'])
            ->setCoordinates($data['coordinates'])
            ->setPrice($data['price']);

        return $object;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createNewCottage(array $data) {
        /** @var Cottage $cottage */
        $cottage = new Cottage();
        $cottage
            ->setType(self::NAME_COTTAGE)
            ->setSauna($data['sauna'])
            ->setCook($data['cook']);
        return $cottage;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createNewPension(array $data) {
        /** @var Pension $pension */
        $pension = new Pension();
        $pension
            ->setType(self::NAME_PENSION)
            ->setMudBath($data['mud_bath'])
            ->setThreeMealsADay($data['three_meals_a_day']);
        return $pension;
    }

}