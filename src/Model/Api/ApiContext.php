<?php

namespace App\Model\Api;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_CONCRETE_CLIENT_BY_EMAIL = '/client_by_email/email/{email}';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/check_client_credentials/{passwordHash}/{email}';
    const ENDPOINT_CHECK_CLIENT_BY_NETWORK_UID = '/check_client_by_network_uid/{network}/{uid}';
    const ENDPOINT_EDIT_CONCRETE_CLIENT_ADD_SOC_BINDING = '/client/edit/add_soc_binding';
    const ENDPOINT_CONCRETE_CLIENT_BY_NETWORK_UID = '/concrete_client_by_network_uid/{network}/{uid}';
    const ENDPOINT_CREATE_NEW_BOOKING_OBJECT = '/booking/create_new_booking_object';
    const ENDPOINT_GET_ALL_OBJECTS = '/booking/get_all_objects';
    const ENDPOINT_GET_FILTER_OBJECTS = '/booking/get_filter_objects';
    const ENDPOINT_GET_OBJECT_BY_ID = '/booking/get_object_by_id/{id}';
    const ENDPOINT_CHECK_BOOKING_BY_PARAM = '/online_booking/check_concrete_booking/{passport}';
    const ENDPOINT_CREATE_BOOKING = '/online_booking/create_booking';
    const ENDPOINT_GET_ALL_BOOKINGS_BY_OBJECT = '/online_booking/get_bookings_by_object/{id}';

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
            'passport' => $passport,
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param $userPassport
     * @return bool
     * @throws ApiException
     */
    public function checkBookingByParam($userPassport)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_BOOKING_BY_PARAM, [
            'passport' => $userPassport
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createBookingByParams(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CREATE_BOOKING, self::METHOD_POST, $data);
    }

    /**
     * @param string $id
     * @return mixed
     * @throws ApiException
     */
    public function getBookingsByObjectId($id)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_GET_ALL_BOOKINGS_BY_OBJECT, [
            'id' => $id
        ]);
        return $this->makeQuery($endPoint, self::METHOD_GET);
    }


    /**
     * @param string $password
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function checkClientCredentials(string $password, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_CREDENTIALS, [
            'passwordHash' => $this->userHandler->encodePassword($password),
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $network
     * @param string $uid
     * @return bool
     * @throws ApiException
     */
    public function checkClientByNetworkUId(string $network, string $uid)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_BY_NETWORK_UID, [
            'network' => $network,
            'uid' => $uid
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param $network
     * @param $uid
     * @return array
     * @throws ApiException
     */
    public function getClientByNetworkUId($network, $uid)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_NETWORK_UID, [
            'network' => $network,
            'uid' => $uid
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param $id
     * @return array
     * @throws ApiException
     */
    public function getObjectById($id)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_GET_OBJECT_BY_ID, [
            'id' => $id
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param $email
     * @return array
     * @throws ApiException
     */
    public function getClientByEmail($email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @return array
     * @throws ApiException
     */
    public function getAllObjects()
    {
        $endPoint = self::ENDPOINT_GET_ALL_OBJECTS;
        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param $data
     * @return array
     * @throws ApiException
     */
    public function getFilterObjectsData($data)
    {
        return $this->makeQuery(self::ENDPOINT_GET_FILTER_OBJECTS, self::METHOD_GET, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createNewObject(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CREATE_NEW_BOOKING_OBJECT, self::METHOD_POST, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function addSocialBindingToTheClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_EDIT_CONCRETE_CLIENT_ADD_SOC_BINDING, self::METHOD_PUT, $data);
    }
}